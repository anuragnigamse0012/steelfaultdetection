import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier


class SteelFaultDetection():

    def __init__(self):
        self.train_accuracy = []
        self.test_accuracy = []

    def read_data(self,file):
        data = pd.read_csv(file)
        return data

    def hotEncodingToLabel(self,data):
        target_names = []
        label_columns = data.columns.values[-7:]
        targets = (data.iloc[:, -7:]).idxmax(1)
        dataset = data.drop(label_columns, axis=1)
        dataset['target'] = targets

        return dataset

    def split_train_test(self,dataset):

        x, y = dataset.loc[:, dataset.columns != 'target'], dataset.loc[:, 'target']
        # need to scale the values of the features since they have different values
        standardScaler = StandardScaler()
        x = standardScaler.fit_transform(x)
        x_train, x_test, y_train, y_test = train_test_split(x, y, stratify=y, test_size=0.6, random_state=42)
        trees = np.arange(1, 50)
        plt.figure(figsize=[8, 5])
        #plt.scatter(x_test,y_test)

        return x_train,x_test, y_train, y_test, trees



    def plot_train_test(self,x_train,x_test, y_train, y_test, trees):

        for i, k in enumerate(trees):
            # k from 1 to 26
            rf = RandomForestClassifier(random_state=8, n_estimators=k, min_samples_split=2)
            # Fit with random Forest
            rf.fit(x_train, y_train)
            # train accuracy
            self.train_accuracy.append(rf.score(x_train, y_train))
            # test accuracy
            self.test_accuracy.append(rf.score(x_test, y_test))
        plt.figure(figsize=[8, 5])
        plt.plot(trees, self.test_accuracy, label='Testing Accuracy')
        plt.plot(trees, self.train_accuracy, label='Training Accuracy')
        plt.legend()
        plt.title('No. of trees VS Accuracy')
        plt.xlabel('Number of Trees')
        plt.ylabel('Accuracy')
        plt.xticks(trees)
        plt.savefig('static/data_plot.png')

    def randomForestClassifier(self, x_train,x_test, y_train, y_test, trees):
        for i, k in enumerate(trees):
            # k from 1 to 26
            rf = RandomForestClassifier(random_state=8, n_estimators=k, min_samples_split=2)
            # Fit with random Forest
            rf.fit(x_train, y_train)
            # train accuracy
            self.train_accuracy.append(rf.score(x_train, y_train))
            # test accuracy
            self.test_accuracy.append(rf.score(x_test, y_test))

        # Plot
        plt.figure(figsize=[8, 5])
        plt.plot(trees, self.test_accuracy, label='Testing Accuracy')
        plt.plot(trees, self.train_accuracy, label='Training Accuracy')
        plt.legend()
        plt.title('No. of trees VS Accuracy')
        plt.xlabel('Number of Trees')
        plt.ylabel('Accuracy')
        plt.xticks(trees)
        plt.savefig('static/graph_1.png')

        rf = RandomForestClassifier(random_state=8, n_estimators=48, min_samples_split=2)
        y_pred = rf.fit(x_train, y_train).predict(x_test)
        cm = confusion_matrix(y_test, y_pred)
        dataPoint = x_test.size
        plt.figure(figsize=[8, 5])
        sns.heatmap(cm, annot=True, fmt='d')
        plt.title('Confusion Matrix')
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.savefig('static/graph_2.png')
        target_names = ['V28','V29','V30','V31','V32','V33','Class']
        classfication_report = classification_report(y_test, y_pred, output_dict = True, target_names=target_names)
        df = pd.DataFrame(classfication_report).transpose()
        return format(np.max(self.test_accuracy)), df, dataPoint

    def svm(self, x_train,x_test, y_train, y_test, trees):

        for i, k in enumerate(trees):
            # k from 1 to 26
            svclassifier = SVC(kernel='linear')
            svclassifier.fit(x_train, y_train)
            # train accuracy
            self.train_accuracy.append(svclassifier.score(x_train, y_train))
            # test accuracy
            self.test_accuracy.append(svclassifier.score(x_test, y_test))

        # Plot
        plt.figure(figsize=[6, 4])
        plt.plot(trees, self.test_accuracy, label='Testing Accuracy')
        plt.plot(trees, self.train_accuracy, label='Training Accuracy')
        plt.legend()
        plt.title('No. of trees VS Accuracy')
        plt.xlabel('Number of Trees')
        plt.ylabel('Accuracy')
        plt.xticks(trees)
        plt.savefig('static/graph_1.png')

        svclassifier = SVC(kernel='linear')
        y_pred = svclassifier.fit(x_train, y_train).predict(x_test)
        cm = confusion_matrix(y_test, y_pred)
        dataPoint = x_test.size
        plt.figure(figsize=[6, 4])
        sns.heatmap(cm, annot=True, fmt='d')
        plt.title('Confusion Matrix')
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.savefig('static/graph_2.png')
        target_names = ['V28', 'V29', 'V30', 'V31', 'V32', 'V33', 'Class']
        classfication_report = classification_report(y_test, y_pred, output_dict=True, target_names=target_names)
        df = pd.DataFrame(classfication_report).transpose()
        return format(np.max(self.test_accuracy)), df, dataPoint

    def logisticRegression(self, x_train,x_test, y_train, y_test, trees):

        for i, k in enumerate(trees):
            # k from 1 to 26
            logRegr = LogisticRegression()
            logRegr.fit(x_train, y_train)
            # train accuracy
            self.train_accuracy.append(logRegr.score(x_train, y_train))
            # test accuracy
            self.test_accuracy.append(logRegr.score(x_test, y_test))

            # Plot

        plt.figure(figsize=[13, 8])
        plt.plot(trees, self.test_accuracy, label='Testing Accuracy')
        plt.plot(trees, self.train_accuracy, label='Training Accuracy')
        plt.legend()
        plt.title('No. of trees VS Accuracy')
        plt.xlabel('Number of Trees')
        plt.ylabel('Accuracy')
        plt.xticks(trees)
        plt.savefig('static/graph_1.png')

        logRegr = LogisticRegression()
        y_pred = logRegr.fit(x_train, y_train).predict(x_test)
        cm = confusion_matrix(y_test, y_pred)
        dataPoint = x_test.size
        plt.figure(figsize=[13, 8])
        sns.heatmap(cm, annot=True, fmt='d')
        plt.title('Confusion Matrix')
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.savefig('static/graph_2.png')
        target_names = ['V28', 'V29', 'V30', 'V31', 'V32', 'V33', 'Class']
        classfication_report = classification_report(y_test, y_pred, output_dict=True, target_names=target_names)
        df = pd.DataFrame(classfication_report).transpose()
        return format(np.max(self.test_accuracy)), df, dataPoint

    def KNN(self,  x_train,x_test, y_train, y_test, trees):

        for i, k in enumerate(trees):
            # k from 1 to 26
            kNN = KNeighborsClassifier(n_neighbors=3)
            kNN.fit(x_train, y_train)
            # train accuracy
            self.train_accuracy.append(kNN.score(x_train, y_train))
            # test accuracy
            self.test_accuracy.append(kNN.score(x_test, y_test))

            # Plot
        plt.figure(figsize=[13, 8])
        plt.plot(trees, self.test_accuracy, label='Testing Accuracy')
        plt.plot(trees, self.train_accuracy, label='Training Accuracy')
        plt.legend()
        plt.title('No. of trees VS Accuracy')
        plt.xlabel('Number of Trees')
        plt.ylabel('Accuracy')
        plt.xticks(trees)
        plt.savefig('static/graph_1.png')


        kNN = KNeighborsClassifier(n_neighbors=3)
        y_pred = kNN.fit(x_train, y_train).predict(x_test)
        dataPoint = x_test.size
        cm = confusion_matrix(y_test, y_pred)
        plt.figure(figsize=[13, 8])
        sns.heatmap(cm, annot=True, fmt='d')
        plt.title('Confusion Matrix')
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.savefig('static/graph_2.png')
        target_names = ['V28', 'V29', 'V30', 'V31', 'V32', 'V33', 'Class']
        classfication_report = classification_report(y_test, y_pred, output_dict=True, target_names=target_names)
        df = pd.DataFrame(classfication_report).transpose()
        return format(np.max(self.test_accuracy)), df, dataPoint

    def mlp(self, x_train,x_test, y_train, y_test, trees):
        for i, k in enumerate(trees):
            # k from 1 to 26
            mlp = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)
            mlp.fit(x_train, y_train)
            # train accuracy
            self.train_accuracy.append(mlp.score(x_train, y_train))
            # test accuracy
            self.test_accuracy.append(mlp.score(x_test, y_test))

            # Plot
        plt.figure(figsize=[13, 8])
        plt.plot(trees, self.test_accuracy, label='Testing Accuracy')
        plt.plot(trees, self.train_accuracy, label='Training Accuracy')
        plt.legend()
        plt.title('No. of trees VS Accuracy')
        plt.xlabel('Number of Trees')
        plt.ylabel('Accuracy')
        plt.xticks(trees)
        plt.savefig('static/graph_2.png')

        mlp =  MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(5, 2), random_state=1)
        y_pred = mlp.fit(x_train, y_train).predict(x_test)
        cm = confusion_matrix(y_test, y_pred)
        dataPoint = x_test.size
        plt.figure(figsize=[13, 8])
        sns.heatmap(cm, annot=True, fmt='d')
        plt.title('Confusion Matrix')
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.savefig('static/graph_2.png')
        target_names = ['V28', 'V29', 'V30', 'V31', 'V32', 'V33', 'Class']
        classfication_report = classification_report(y_test, y_pred, output_dict=True, target_names=target_names)
        df = pd.DataFrame(classfication_report).transpose()
        return format(np.max(self.test_accuracy)), df, dataPoint

