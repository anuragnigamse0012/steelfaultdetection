from flask import Flask, request, render_template
from steelfaultdetection import SteelFaultDetection
import os
app = Flask(__name__)

@app.route('/app')
def start_app():
    return render_template('main.html')

@app.route('/steelfault', methods=['POST'])
def steelfault():
    if os.path.exists('/home/anurag/Desktop/SteelFaultDetection/static/graph_2.png'):
        os.remove('/home/anurag/Desktop/SteelFaultDetection/static/graph_2.png')
        print('sucess')

    file = request.files['file']
    if request.method == 'POST':
        fav = int(request.form['classifier'])
        steel = SteelFaultDetection()
        data = steel.read_data(file)
        convertingTolabel = steel.hotEncodingToLabel(data)
        x_train,x_test, y_train, y_test, trees = steel.split_train_test(convertingTolabel)
        
                if fav == 1:
            classifier = 'Random Forest'
            result, df, datapoint = steel.randomForestClassifier(x_train, x_test, y_train, y_test, trees)
        elif fav == 2:
            classifier = 'K Nearest-Neighbour'
            result, df, datapoint  = steel.KNN(x_train, x_test, y_train, y_test, trees)
        elif fav == 3:
            classifier = 'Support Vector Machine'
            result, df, datapoint  = steel.svm(x_train, x_test, y_train, y_test, trees)
        else:
            classifier = 'Logistic Regression'
            result, df, datapoint = steel.logisticRegression(x_train, x_test, y_train, y_test, trees)


        return render_template('result.html',result = str(result),tables=[df.to_html(classes='data', header="true")], classifier = classifier, datapoint=datapoint)

if __name__ == '__main__':
    app.run(use_reloader=True, debug=True)




